var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
let count = 1;

io.on('connection', function(socket) {
    console.log('A user connected');
    socket.emit('messages', messages);
    setInterval(function(){ 
        socket.emit('messages', `bla bla bla bla ${count++}`);
    }, 1000);
});

server.listen(4545, () => {
    console.log('Socket.io server is listening on port 4545');
});

const messages = [{
	author: "Carlos",
    text: "Hola! que tal?"
},{
	author: "Pepe",
    text: "Muy bien! y tu??"
},{
	author: "Paco",
    text: "Genial!"
}];